﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using System.IO;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace InterfazEstadoCuenta
{


    public partial class FrmInterfaz : Form
    {
        String Cuenta = "";
        String Password = "";
        String Host = "";
        Int32 Port = 0;
        Boolean Bnd = false;
        Int32 Cont = 0;
        Int32 Total = 0;
        Int64 Contrato = 0;
        String Email = String.Empty;
        DateTime Fecha;
        String Asunto = "";
        String Mensaje = "";
        String Adjunto = "";
        String MensajeDeco = "";
        Boolean Correo = true;
        String Msj = String.Empty;
        Int32 GloIdCompania = 0;
        Int64 ID = 0;
        public FrmInterfaz()
        {
            InitializeComponent();
        }

        public void ConGeneralCorreo()
        {
            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            SqlCommand comando = new SqlCommand("ConGeneralCorreo", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;

            SqlDataReader reader;

            try
            {
                conexion.Open();
                reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    Cuenta = reader[0].ToString();
                    Password = reader[1].ToString();
                    Host = reader[2].ToString();
                    Port = Int32.Parse(reader[3].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }
        }

        public void MandarEstadoCuentaCorreo()
        {

            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            SqlCommand comando = new SqlCommand("MandarEstadoCuentaCorreo", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;
            SqlDataReader reader;

            try
            {
                conexion.Open();
                reader = comando.ExecuteReader();
                Bnd = true;
                Cont = 0;

                while (reader.Read())
                {
                    try
                    {
                        ConGeneralCorreo();
                        ConEstadoCuentaMensaje();

                        Contrato = Int64.Parse(reader[0].ToString());
                        Email = reader[1].ToString();
                        Fecha = DateTime.Parse(reader[2].ToString());
                        Total = Int32.Parse(reader[3].ToString());
                        ID = Int64.Parse(reader[4].ToString());
                      

                        if (Bnd == true)
                        {
                            Bnd = false;
                        }

                        Cont += 1;
                        lblStatus.Text = "Enviando correos" + Cont.ToString();
                        lblStatus.ForeColor = Color.Red;
                        lblStatus.Refresh();

                        ConEstadoCuentaCorreo(Contrato, Fecha, ID);

                      


                        using (MailMessage mailMessage = new MailMessage())
                        using (Attachment attachment = new Attachment(VariablesGlobales .Ruta + @"\EstadosCuenta\EdoCuenta.pdf"))
                        {
                            SmtpClient smtpClient = new SmtpClient();
                            mailMessage.From = new MailAddress(Cuenta);
                            foreach (var address in Email.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                mailMessage.To.Add(address);
                            }
                            //mailMessage.To.Add(Email);
                            mailMessage.Subject = Mensaje;
                            mailMessage.Body = Asunto;
                            mailMessage.IsBodyHtml = true;
                            mailMessage.Attachments.Add(attachment);

                            smtpClient.UseDefaultCredentials = false;
                            smtpClient.Credentials = new System.Net.NetworkCredential(Cuenta, Password);
                            smtpClient.Port = Port;
                            smtpClient.Host = Host;
                            smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                            smtpClient.EnableSsl = true;
                            ServicePointManager.ServerCertificateValidationCallback = (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => true;
                            smtpClient.Send(mailMessage);

                            NueEstadoCuentaBitacora(Contrato, Fecha, Email, true, "");
                            ModEstadoCuentaStatus(ID, Contrato);
                        }
                    }
                    catch (Exception ex)
                    {
                        ModEstadoCuentaStatusError(ID, Contrato);
                        NueEstadoCuentaBitacora(Contrato, Fecha, Email, false, ex.ToString());
                    }
                }

                lblStatus.Text = "";
                lblStatus.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }

        }





 







        public void ConEstadoCuentaCorreo(Int64 Contrato, DateTime Fecha, Int64 ID)
        {

            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());

            ReportDocument reportDocument = new ReportDocument();
            String lineaOxxo;
            String Datalogic;



            try
            {

                if (conexion.State == ConnectionState.Closed)
                    conexion.Open();



                //Para generar los códigos de barras
                //SqlCommand comando = new SqlCommand("DameLineaOxxo");
                //comando.CommandType = CommandType.StoredProcedure;
                //comando.CommandTimeout = 0;
                //comando.Connection = conexion;

                //SqlParameter par1 = new SqlParameter("@idEstadoCuenta", SqlDbType.BigInt);
                //par1.Value = ID;
                //par1.Direction = ParameterDirection.Input;
                //comando.Parameters.Add(par1);

                //SqlParameter par3 = new SqlParameter("@lineaOxxo", SqlDbType.VarChar, 40);
                //par3.Direction = ParameterDirection.Output;
                //comando.Parameters.Add(par3);

                //SqlParameter par4 = new SqlParameter("@Datalogic", SqlDbType.VarChar, 40);
                //par4.Direction = ParameterDirection.Output;
                //comando.Parameters.Add(par4);

                //comando.ExecuteNonQuery();
                //lineaOxxo = par3.Value.ToString();
                //Datalogic = par4.Value.ToString();

                //Byte[] imgOxxo = GeneraCodeBar(lineaOxxo);
                //Byte[] imgDataL = GeneraCodeBar(Datalogic);




                //Fin para generar estado de cuenta
                SqlCommand strSQL = new SqlCommand("ConEstadoCuentaAImprimir");
                strSQL.Connection = conexion;
                strSQL.CommandType = CommandType.StoredProcedure;
                strSQL.CommandTimeout = 0;

                SqlParameter par10 = new SqlParameter("@OP", SqlDbType.Int);
                par10.Value = 1;
                par10.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par10);




                SqlParameter par30 = new SqlParameter("@CONTRATO", SqlDbType.Int);
                par30.Value = Contrato;
                par30.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par30);



                SqlParameter par60 = new SqlParameter("@IDESTADOCUENTA", SqlDbType.BigInt);
                par60.Value = ID;
                par60.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par60);



                //SqlParameter par50 = new SqlParameter("@imageOxxo", SqlDbType.Image);
                //par50.Value = imgOxxo;
                //par50.Direction = ParameterDirection.Input;
                //strSQL.Parameters.Add(par50);


                //SqlParameter par70 = new SqlParameter("@ImgDatL", SqlDbType.Image);
                //par70.Value = imgDataL;
                //par70.Direction = ParameterDirection.Input;
                //strSQL.Parameters.Add(par70);



                SqlDataAdapter dataAdapter = new SqlDataAdapter(strSQL);


                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataSet);

                //dataTable = MuestraGeneral();
                dataSet.Tables[0].TableName = "EstadoCuenta";
                dataSet.Tables[1].TableName = "DetEstadoCuenta";
                // dataSet.Tables.Add(dataTable);
                dataSet.Tables[2].TableName = "General";
                //dataSet.Tables[0].TableName = "EstadoCuenta";
                //dataSet.Tables[1].TableName = "DetEstadoCuenta";

                reportDocument.Load(VariablesGlobales.Ruta + @"\Repot2.rpt");
                reportDocument.SetDataSource(dataSet);
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, VariablesGlobales.Ruta + @"\EstadosCuenta\EdoCuenta.pdf");


                reportDocument.Close();
                reportDocument.Dispose();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
            finally
            {
               conexion.Close();
                conexion.Dispose();
            }
        }







        public void ConEstadoCuenta(Int64 Contrato, DateTime Fecha, Int64 ID)
        {

            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());

            ReportDocument reportDocument = new ReportDocument();
            String lineaOxxo;
            String Datalogic;



            try
            {

                if (conexion.State == ConnectionState.Closed)
                    conexion.Open();



                //Para generar los códigos de barras
                //SqlCommand comando = new SqlCommand("DameLineaOxxo");
                //comando.CommandType = CommandType.StoredProcedure;
                //comando.CommandTimeout = 0;
                //comando.Connection = conexion;

                //SqlParameter par1 = new SqlParameter("@idEstadoCuenta", SqlDbType.BigInt);
                //par1.Value = ID;
                //par1.Direction = ParameterDirection.Input;
                //comando.Parameters.Add(par1);

                //SqlParameter par3 = new SqlParameter("@lineaOxxo", SqlDbType.VarChar, 40);
                //par3.Direction = ParameterDirection.Output;
                //comando.Parameters.Add(par3);

                //SqlParameter par4 = new SqlParameter("@Datalogic", SqlDbType.VarChar, 40);
                //par4.Direction = ParameterDirection.Output;
                //comando.Parameters.Add(par4);

                //comando.ExecuteNonQuery();
                //lineaOxxo = par3.Value.ToString();
                //Datalogic = par4.Value.ToString();

                //Byte[] imgOxxo = GeneraCodeBar(lineaOxxo);
                //Byte[] imgDataL = GeneraCodeBar(Datalogic);




                //Fin para generar estado de cuenta
                SqlCommand strSQL = new SqlCommand("ConEstadoCuentaAImprimir");
                strSQL.Connection = conexion;
                strSQL.CommandType = CommandType.StoredProcedure;
                strSQL.CommandTimeout = 0;

                SqlParameter par10 = new SqlParameter("@OP", SqlDbType.Int);
                par10.Value = 1;
                par10.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par10);




                SqlParameter par30 = new SqlParameter("@CONTRATO", SqlDbType.Int);
                par30.Value = Contrato;
                par30.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par30);



                SqlParameter par60 = new SqlParameter("@IDESTADOCUENTA", SqlDbType.BigInt);
                par60.Value = ID;
                par60.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par60);



                //SqlParameter par50 = new SqlParameter("@imageOxxo", SqlDbType.Image);
                //par50.Value = imgOxxo;
                //par50.Direction = ParameterDirection.Input;
                //strSQL.Parameters.Add(par50);


                //SqlParameter par70 = new SqlParameter("@ImgDatL", SqlDbType.Image);
                //par70.Value = imgDataL;
                //par70.Direction = ParameterDirection.Input;
                //strSQL.Parameters.Add(par70);



                SqlDataAdapter dataAdapter = new SqlDataAdapter(strSQL);


                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataSet);

                //dataTable = MuestraGeneral();
                dataSet.Tables[0].TableName = "EstadoCuenta";
                dataSet.Tables[1].TableName = "DetEstadoCuenta";
                // dataSet.Tables.Add(dataTable);
                dataSet.Tables[2].TableName = "General";
                //dataSet.Tables[0].TableName = "EstadoCuenta";
                //dataSet.Tables[1].TableName = "DetEstadoCuenta";

                reportDocument.Load(VariablesGlobales.Ruta + @"\Repot2.rpt");
                reportDocument.SetDataSource(dataSet);
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, VariablesGlobales.Ruta + @"\EstadosCuentaPDF\EdoCuenta_"+ Contrato+".pdf");


                reportDocument.Close();
                reportDocument.Dispose();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }
        }








        public Byte[] GeneraCodeBar(String Data)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            Byte[] imgBarCode = null;
            PictureBox barcode = new PictureBox();
            Int32 W = 350;
            Int32 H = 100;
            BarcodeLib.AlignmentPositions Align;
            Align = BarcodeLib.AlignmentPositions.CENTER;
            BarcodeLib.TYPE type;
            type = BarcodeLib.TYPE.CODE128;
            try
            {
                b.IncludeLabel = true;
                b.Alignment = Align;
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone);
                //b.LabelPosition=LabelPosition.BOTTOMCENTER;
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H);
                barcode.Width = barcode.Image.Width;
                barcode.Height = barcode.Image.Height;
                Byte[] imgbites;
                imgbites = ImageToByte2(barcode.Image);
                imgBarCode = imgbites;
                return imgBarCode;
            }
            catch (Exception ex)
            {

            }
            //return imgBarCode;
            return imgBarCode;
        }

        public Byte[] ImageToByte2(Image img)
        {
            Byte[] byteArray;
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        public DataTable  MuestraGeneral() {
            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            StringBuilder stringBuilder = new StringBuilder("EXEC MuestraGeneral");
            SqlDataAdapter dataAdapter = new SqlDataAdapter(stringBuilder.ToString(), conexion);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            return dataTable;
        }

        public void NueEstadoCuentaBitacora(Int64 Contrato, DateTime Fecha, String Email, Boolean Exito, String Error)
        {
            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            SqlCommand comando = new SqlCommand("NueEstadoCuentaBitacora", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;

            SqlParameter par1 = new SqlParameter("@Contrato", SqlDbType.BigInt);
            par1.Value = Contrato;
            par1.Direction = ParameterDirection.Input;
            comando.Parameters.Add(par1);

            SqlParameter par2 = new SqlParameter("@Fecha", SqlDbType.DateTime);
            par2.Value = Fecha.ToShortDateString();
            par2.Direction = ParameterDirection.Input;
            comando.Parameters.Add(par2);

            SqlParameter par3 = new SqlParameter("@Email", SqlDbType.VarChar, 100);
            par3.Value = Email;
            par3.Direction = ParameterDirection.Input;
            comando.Parameters.Add(par3);

            SqlParameter par4 = new SqlParameter("@Exito", SqlDbType.Bit);
            par4.Value = Exito;
            par4.Direction = ParameterDirection.Input;
            comando.Parameters.Add(par4);

            SqlParameter par5 = new SqlParameter("@Error", SqlDbType.VarChar, 750);
            par5.Value = Error;
            par5.Direction = ParameterDirection.Input;
            comando.Parameters.Add(par5);

            try
            {
                conexion.Open();
                comando.ExecuteNonQuery();


            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conexion != null)
                    conexion.Close();
                //conexion.Close();
                //conexion.Dispose();
            }
        }

        public void ModEstadoCuentaStatus(Int64 ID, Int64 Contrato)
        {
            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            SqlCommand comando = new SqlCommand("exec ModEstadoCuentaStatus " + ID.ToString(), conexion);
            comando.CommandTimeout = 0;

            try
            {
                conexion.Open();
                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }
        }

        public void ModEstadoCuentaStatusError(Int64 ID, Int64 Contrato)
        {
            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            SqlCommand comando = new SqlCommand("exec ModEstadoCuentaStatusError " + ID.ToString(), conexion);
            comando.CommandTimeout = 0;

            try
            {
                conexion.Open();
                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }
        }

        public void ConEstadoCuentaMensaje()
        {
            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            SqlCommand comando = new SqlCommand("ConEstadoCuentaMensaje", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;

            SqlDataReader reader;

            try
            {
                conexion.Open();
                reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    Asunto = reader[0].ToString();
                    Mensaje = reader[1].ToString();
                    //Adjunto = reader[2].ToString();
                    //MensajeDeco = reader[3].ToString();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }
        }

        public void ChecaMandarEstadoCuentaCorreo()
        {
            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
            SqlCommand comando = new SqlCommand("ChecaMandarEstadoCuentaCorreo", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;

            SqlDataReader reader;

            try
            {
                conexion.Open();
                reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    Correo = true;
                    Msj = String.Empty;
                    Correo = Boolean.Parse (reader[0].ToString());
                    Msj = reader[1].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }
        }

        private void bIniciar_Click(object sender, EventArgs e)
        {
            //ChecaMandarEstadoCuentaCorreo();
            if (Correo == true)
            {
                bIniciar.Enabled = false;
                lblStatus.Visible = true;
                timer.Enabled = true;
            }
            else
            {
                MessageBox.Show(Msj);   
            }
        }

        private void bDetener_Click(object sender, EventArgs e)
        {
            if (VariablesGlobales.Work == true)
            {
                bIniciar.Enabled = true;
                lblStatus.Text = "Activa...";
                lblStatus.ForeColor = Color.Blue;
                lblStatus.Refresh();
                lblStatus.Visible = false;
                timer.Enabled = false;
            }
            else
            {
                MessageBox.Show("La interfaz está en ejecución, no se detendrá.");
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (VariablesGlobales.Work == true)
                {
                    VariablesGlobales.Work = false;

                    MandarEstadoCuentaCorreo();

                    lblStatus.Text = "Activa...";
                    lblStatus.ForeColor = Color.Blue;
                    lblStatus.Refresh();

                    VariablesGlobales.Work = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmInterfaz_Load(object sender, EventArgs e)
        {

        }




        private void button1_Click(object sender, EventArgs e)
        {




            {

                SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Miconexion"].ToString());
                SqlCommand comando = new SqlCommand("MandarEstadoCuentaCorreo", conexion);
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandTimeout = 0;
                SqlDataReader reader;

                try
                {
                    conexion.Open();
                    reader = comando.ExecuteReader();
                    Bnd = true;
                    Cont = 0;
                    lblStatus.Visible = true;
                    bIniciar.Enabled = false;
                    //  bDetener.Enabled = false;

                    while (reader.Read())
                    {
                        try
                        {


                            Contrato = Int64.Parse(reader[0].ToString());
                            Email = reader[1].ToString();
                            Fecha = DateTime.Parse(reader[2].ToString());
                            Total = Int32.Parse(reader[3].ToString());
                            ID = Int64.Parse(reader[4].ToString());


                            if (Bnd == true)
                            {
                                Bnd = false;
                            }

                            Cont += 1;
                            lblStatus.Text = "Generando PDF'S......... " + Cont.ToString();
                            lblStatus.ForeColor = Color.Red;
                            lblStatus.Refresh();


                            ConEstadoCuenta(Contrato, Fecha, ID);


                        }
                        catch (Exception ex)
                        {
                            ModEstadoCuentaStatusError(ID, Contrato);
                            NueEstadoCuentaBitacora(Contrato, Fecha, Email, false, ex.ToString());
                        }
                    }

                    lblStatus.Text = "";
                    lblStatus.Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    conexion.Close();
                    conexion.Dispose();
                }

            }

         }

            private void lblStatus_Click(object sender, EventArgs e)
        {

        }

        //private void button1_Click(object sender, EventArgs e)
        //{

        //}
    }
}
