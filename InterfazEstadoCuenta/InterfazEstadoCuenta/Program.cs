﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace InterfazEstadoCuenta
{
    class Program
    {
        public static void Main(String[] str)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmInterfaz ());

        }
    }
}
